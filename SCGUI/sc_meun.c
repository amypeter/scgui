#include "sc_meun.h"
#include "sc_event_task.h"
#include "string.h"
void home_sel_key  (void *arg,u8 init);
void nianji_sel_key(void *arg,u8 init);
void nandu_sel_key (void *arg,u8 init);

void meun_list0_key(void *arg,u8 init);
void meun_list1_key(void *arg,u8 init);
void meun_list2_key(void *arg,u8 init);
void meun_list3_key(void *arg,u8 init);
void meun_list4_key(void *arg,u8 init);
void meun_list5_key(void *arg,u8 init);


 sc_meun  g_home= {"首页",(SC_img_t*)&png_home,NULL,home_sel_key};
//---------------下级子菜-------------------------
 sc_meun  meun_list[]=
{
    {"成语学习",(SC_img_t*)&png0_sel, &g_home,nianji_sel_key},   //二极选择菜单
    {"释义成语",(SC_img_t*)&png1,     &g_home,meun_list1_key},
    {"成语解析",(SC_img_t*)&png2,     &g_home,meun_list2_key},
    {"成语故事",(SC_img_t*)&png3,     &g_home,meun_list3_key},
    {"成语填空",(SC_img_t*)&png4_sel, &g_home,nandu_sel_key},   //二极选择菜单
    {"诗词填空",(SC_img_t*)&png5,     &g_home,meun_list5_key},

    {"成语选关",(SC_img_t*)&png0,     &g_home,meun_list0_key},
    {"填空选关",(SC_img_t*)&png4,     &g_home,meun_list4_key},
};

 sc_meun *g_meun=&g_home;

void home_sel_key(void *arg,u8 init)
{
    sc_event_t *e=arg;
    static SC_AERA box= {.xs=0,.ys=0,.xe=100,.ye=100};
    static int x=4,y=26;
    static int wherr=0;   //六宫格
    if(init)
    {
        if(gui->gbkg!=g_meun->png)   SC_Show_Image(0,0,g_meun->png,1);
        gui->alpha=160;
        SC_RoundFrame_AA(box.xs+x,box.ys+y,box.xe+x,box.ye+y,20,18,C_RED,C_WHEAT);
        return;
    }
    switch(e->key)
    {
    case KEY_EVENT_UP:
    case KEY_EVENT_DP:
        if(wherr<3)
            wherr+=3;
        else
            wherr-=3;
        break;
    case KEY_EVENT_L:
        if(wherr>0) wherr--;
        else
            wherr=5;
        break;
    case KEY_EVENT_R:
        if(++wherr>5) wherr=0;
        break;
    case KEY_EVENT_OK:
        g_meun=&meun_list[wherr];
        SC_Show_Image(0,0,g_meun->png,1);
        g_meun->meun_key(e,1);
        break;
    }
    //------------先清后写----------------------------
    if(e->key&(KEY_EVENT_UP|KEY_EVENT_DP|KEY_EVENT_L|KEY_EVENT_R))
    {
        gui->alpha=0;
        SC_RoundFrame_AA(box.xs+x,box.ys+y,box.xe+x,box.ye+y,20,18,C_RED,C_WHEAT);
        gui->alpha=160;
        x=(wherr%3)*104+4;
        y=(wherr/3)*104+24;
        SC_RoundFrame_AA(box.xs+x,box.ys+y,box.xe+x,box.ye+y,20,18,C_RED,C_WHEAT);
    }
}

const char *sel_str[]=
{
    "1-3年级",
    "4-6年级",
    "初中",
    "高中",
    "叠字成语",
    "历史故事成语",
    "寓言神话成语",
    "写作常用成语",
};
//年级选择
u8 levl_list0;
void nianji_sel_key (void *arg,u8 init)
{
    sc_event_t *e=arg;

    SC_AERA text;
    static SC_AERA box= {.xs=0,.ys=0,.xe=115,.ye=28};
    static int x,y,wherr=0;     //8宫格
    uint16_t fc;
    int xoffs=28;
    int yoffs=45;
    u8 i, r=12;

    if(init)
    {
        //-------------刷背影----------
        if(gui->gbkg!=g_meun->png) SC_Show_Image(0,0,g_meun->png,1);
        for(i=0; i<8; i++)
        {
            x=(i%2)*150+xoffs;
            y=(i/2)*40+ yoffs;
            if(i==wherr)
            {
                gui->alpha=120;
                fc=C_RED;
            }
            else
            {
                gui->alpha=40;
                fc=C_BLUE;
            }
            SC_RoundFrame_half(box.xs+x,box.ys+y,box.xe+x,box.ye+y,r,0,fc,C_WHEAT);
            text.xs=box.xs+x;
            text.ys=box.ys+y;
            text.xe=box.xe+x;
            text.ye=box.xe+x;
            SC_text_align(&text,SC_STATE_STR_MID);
            SC_Show_str(0,5,C_BLACK,fc,sel_str[i], 0,gui->font);
        }
        levl_list0=wherr;
        return;
    }
    switch(e->key)
    {
    case KEY_EVENT_UP:
        wherr-=2;
        if(wherr<0) wherr+=8;
        break;
    case KEY_EVENT_DP:
        wherr+=2;
        if(wherr>7) wherr-=8;
        break;
    case KEY_EVENT_L:
        if(--wherr<0) wherr=7;
        break;
    case KEY_EVENT_R:
        if(++wherr>7) wherr=0;
        break;
    case KEY_EVENT_OK:
        g_meun=&meun_list[6];
        SC_Show_Image(0,0,g_meun->png,1);
        g_meun->meun_key(e,1);
        break;
    }
    //------------先清后写----------------------------
    if(e->key&(KEY_EVENT_UP|KEY_EVENT_DP|KEY_EVENT_L|KEY_EVENT_R))
    {
        for(i=0; i<8; i++)
        {
            if(i==wherr)
            {
                gui->alpha=120;
                fc=C_RED;
            }
            else    if(i==levl_list0)
            {
                gui->alpha=40;
                fc=C_BLUE;
            }
            else
            {
                continue;
            }
            x=(i%2)*150+xoffs;
            y=(i/2)*40+ yoffs;
            SC_RoundFrame_half(box.xs+x,box.ys+y,box.xe+x,box.ye+y,r,0,fc,C_WHEAT);
            text.xs=box.xs+x;
            text.ys=box.ys+y;
            text.xe=box.xe+x;
            text.ye=box.xe+x;
            SC_text_align(&text,SC_STATE_STR_MID);
            SC_Show_str(0,5,C_BLACK,fc,sel_str[i], 0,gui->font);
        }
        levl_list0=wherr;
    }
}


const topic_t test=
{
    3,              //答案
    "找对应的成语", //标题
    "成语1",
    "成语2",
    "成语3",
    "成语4",
};

void meun_list0_key(void *arg,u8 init)
{
    sc_event_t *e=arg;
    static SC_AERA box= {.xs=0,.ys=0,.xe=100,.ye=30};
    static int x,y,wherr=0,last;         //4宫格
    uint16_t fc;
    int xoffs=50;
    int yoffs=105;
    u8 i, r=0;
    SC_AERA text;
    const topic_t *p=&test;
    if(init)
    {
        //-------------刷背影----------
        if(gui->gbkg!=g_meun->png) SC_Show_Image(0,0,g_meun->png,1);

        SC_text_align(&gui->lcd_area,SC_STATE_STR_MID);

        SC_Show_str(0,50,C_BLACK,0,p->title, 0,gui->font);
        for(i=0; i<4; i++)
        {
            x=(i%2)*120+ xoffs;
            y=(i/2)*40+ yoffs;
            if(i==wherr)
            {
                gui->alpha=120;
                fc=C_RED;
            }
            else
            {
                gui->alpha=40;
                fc=C_BLUE;
            }
            SC_RoundFrame_AA(box.xs+x,box.ys+y,box.xe+x,box.ye+y,r,0,fc,C_WHEAT);
            text.xs=box.xs+x;
            text.ys=box.ys+y;
            text.xe=box.xe+x;
            text.ye=box.xe+x;
            SC_text_align(&text,SC_STATE_STR_LEFT);

            SC_Show_str(0,5,C_BLACK,fc,p->content[i], 0,gui->font);
        }
        last=wherr;
        return;
    }
    switch(e->key)
    {
    case KEY_EVENT_L:
        wherr--;
        if(wherr<0) wherr=3;
        break;
    case KEY_EVENT_R:
        wherr++;
        if(wherr>=4) wherr=0;
        break;
    case KEY_EVENT_UP:
    case KEY_EVENT_DP:
        wherr+=2;
        if(wherr>=4) wherr-=4;
        break;
    case KEY_EVENT_OK:
        x=(wherr%2)*120+ xoffs;
        y=(wherr/2)*40+ yoffs;
        text.xs=box.xs+x;
        text.ys=box.ys+y;
        text.xe=box.xe+x;
        text.ye=box.xe+x;
        SC_text_align(&text,SC_STATE_STR_RIGHT);
        gui->alpha=120;

        if(wherr==p->answer)
        {
          SC_Show_str(0,5,C_BLACK,C_RED,"Y", 0,gui->font);
        }
        else
        {
          SC_Show_str(0,5,C_BLACK,C_RED,"N", 0,gui->font);
        }

        break;
    }
    if(e->key&(KEY_EVENT_UP|KEY_EVENT_DP|KEY_EVENT_L|KEY_EVENT_R))
    {
        for(i=0; i<8; i++)
        {
            x=(i%2)*120+ xoffs;
            y=(i/2)*40+ yoffs;
            if(i==wherr)
            {
                gui->alpha=120;
                fc=C_RED;
            }
            else  if(i==last)
            {
                gui->alpha=40;
                fc=C_BLUE;
            }
            else
            {
                continue;
            }
            SC_RoundFrame_AA(box.xs+x,box.ys+y,box.xe+x,box.ye+y,r,0,fc,C_WHEAT);
            text.xs=box.xs+x;
            text.ys=box.ys+y;
            text.xe=box.xe+x;
            text.ye=box.xe+x;
            SC_text_align(&text,SC_STATE_STR_LEFT);
            SC_Show_str(0,5,C_BLACK,fc,p->content[i], 0,gui->font);
        }
        last=wherr;
    }
}



const topic_t test1=
{
    3,
    "释义选成语",
    "释义1",
    "释义2",
    "释义3",
    "释义4",
};
void meun_list1_key(void *arg,u8 init)
{
    sc_event_t *e=arg;
    static SC_AERA box= {.xs=0,.ys=0,.xe=100,.ye=30};
    static int x,y,wherr=0,last;         //4宫格
    uint16_t fc;
    int xoffs=40;
    int yoffs=105;
    u8 i, r=0;
    SC_AERA text;
    const topic_t *p=&test1;
    if(init)
    {
        //-------------刷背影----------
        if(gui->gbkg!=g_meun->png) SC_Show_Image(0,0,g_meun->png,1);
        SC_text_align(&gui->lcd_area,SC_STATE_STR_MID);
        SC_Show_str(0,50,C_BLACK,0,p->title, 0,gui->font);
        return;
    }
}
const topic_t test2=
{
    3,
    "成语解析",
    "释义1",
    "释义2",
    "释义3",
    "释义4",
};
void meun_list2_key(void *arg,u8 init)
{
    sc_event_t *e=arg;
    static SC_AERA box= {.xs=0,.ys=0,.xe=100,.ye=30};
    static int x,y,wherr=0,last;         //4宫格
    uint16_t fc;
    int xoffs=40;
    int yoffs=105;
    u8 i, r=0;
    SC_AERA text;
    const topic_t *p=&test2;
    if(init)
    {
        //-------------刷背影----------
        if(gui->gbkg!=g_meun->png) SC_Show_Image(0,0,g_meun->png,1);
        SC_text_align(&gui->lcd_area,SC_STATE_STR_MID);
        SC_Show_str(0,50,C_BLACK,0,p->title, 0,gui->font);
        return;
    }
}
const topic_t test3=
{
    3,
    "成语故事",
    "故事1",
    "故事2",
    "故事3",
    "故事4",
};
void meun_list3_key(void *arg,u8 init)
{
    sc_event_t *e=arg;
    static SC_AERA box= {.xs=0,.ys=0,.xe=100,.ye=30};
    static int x,y,wherr=0,last;         //4宫格
    uint16_t fc;
    int xoffs=40;
    int yoffs=105;
    u8 i, r=0;
    SC_AERA text;
    const topic_t *p=&test3;
    if(init)
    {
        //-------------刷背影----------
        if(gui->gbkg!=g_meun->png) SC_Show_Image(0,0,g_meun->png,1);
        SC_text_align(&gui->lcd_area,SC_STATE_STR_MID);
        SC_Show_str(0,50,C_BLACK,0,p->title, 0,gui->font);
        return;
    }
}


//难度选择
const char *sel_str1[]=
{
    "小菜一碟",
    "举步维艰",
    "难如登天",
};

void nandu_sel_key (void *arg,u8 init)
{
    sc_event_t *e=arg;
    static SC_AERA box= {.xs=0,.ys=0,.xe=150,.ye=30};
    static int x,y,wherr=0,last;     //3宫格
    uint16_t fc;
    int xoffs=80;
    int yoffs=65;
    u8 i, r=12;
    SC_AERA text;
    if(init)
    {
        //-------------刷背影----------
        if(gui->gbkg!=g_meun->png) SC_Show_Image(0,0,g_meun->png,1);
        for(i=0; i<3; i++)
        {
            x=xoffs;
            y=(i)*40+ yoffs;
            if(i==wherr)
            {
                gui->alpha=120;
                fc=C_RED;
            }
            else
            {
                gui->alpha=40;
                fc=C_BLUE;
            }
            SC_RoundFrame_AA(box.xs+x,box.ys+y,box.xe+x,box.ye+y,r,0,fc,C_WHEAT);
            text.xs=box.xs+x;
            text.ys=box.ys+y;
            text.xe=box.xe+x;
            text.ye=box.xe+x;
            SC_text_align(&text,SC_STATE_STR_MID);
            SC_Show_str(0,5,C_BLACK,fc,sel_str1[i], 0,gui->font);
        }
        last=wherr;
        return;
    }
    switch(e->key)
    {
    case KEY_EVENT_UP:
        wherr--;
        if(wherr<0) wherr+=3;
        break;
    case KEY_EVENT_DP:
        wherr++;
        if(wherr>2) wherr-=3;
        break;
    case KEY_EVENT_L:
    case KEY_EVENT_R:
        return;
        break;
    case KEY_EVENT_OK:
        g_meun=&meun_list[7];
        SC_Show_Image(0,0,g_meun->png,1);
        g_meun->meun_key(e,1);
        break;
    }
    //------------先清后写----------------------------
    if(e->key&(KEY_EVENT_UP|KEY_EVENT_DP|KEY_EVENT_L|KEY_EVENT_R))
    {
        for(i=0; i<3; i++)
        {
            if(i==wherr)
            {
                gui->alpha=120;
                fc=C_RED;
            }
            else    if(i==last)
            {
                gui->alpha=40;
                fc=C_BLUE;
            }
            else
            {
                continue;
            }
            x=xoffs;
            y=(i)*40+ yoffs;
            SC_RoundFrame_AA(box.xs+x,box.ys+y,box.xe+x,box.ye+y,r,0,fc,C_WHEAT);
            text.xs=box.xs+x;
            text.ys=box.ys+y;
            text.xe=box.xe+x;
            text.ye=box.xe+x;
            SC_text_align(&text,SC_STATE_STR_MID);
            SC_Show_str(0,5,C_BLACK,fc,sel_str1[i], 0,gui->font);
        }
        last=wherr;
    }
}

const topic_t test4=
{
    3,
    "成语填空",
    "故事1",
    "故事2",
    "故事3",
    "故事4",
};
void meun_list4_key(void *arg,u8 init)
{
    sc_event_t *e=arg;
    static SC_AERA box= {.xs=0,.ys=0,.xe=100,.ye=30};
    static int x,y,wherr=0,last;         //4宫格
    uint16_t fc;
    int xoffs=40;
    int yoffs=105;
    u8 i, r=0;
    SC_AERA text;
    const topic_t *p=&test4;
    if(init)
    {
        //-------------刷背影----------
        if(gui->gbkg!=g_meun->png) SC_Show_Image(0,0,g_meun->png,1);
        SC_text_align(&gui->lcd_area,SC_STATE_STR_MID);
        SC_Show_str(0,50,C_BLACK,0,p->title, 0,gui->font);
        return;
    }
}

const topic_t test5=
{
    3,
    "诗词填空",
    "故事1",
    "故事2",
    "故事3",
    "故事4",
};
void meun_list5_key(void *arg,u8 init)
{
    sc_event_t *e=arg;
    static SC_AERA box= {.xs=0,.ys=0,.xe=100,.ye=30};
    static int x,y,wherr=0,last;         //4宫格
    uint16_t fc;
    int xoffs=40;
    int yoffs=105;
    u8 i, r=0;
    SC_AERA text;
    const topic_t *p=&test5;
    if(init)
    {
        //-------------刷背影----------
        if(gui->gbkg!=g_meun->png) SC_Show_Image(0,0,g_meun->png,1);
        SC_text_align(&gui->lcd_area,SC_STATE_STR_MID);
        SC_Show_str(0,50,C_BLACK,0,p->title, 0,gui->font);
        return;
    }
}
