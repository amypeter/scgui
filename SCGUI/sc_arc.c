


#include "sc_gui.h"
#include "sc_arc.h"

#if 0//DEBUG
uint16_t SC_atan2(int x, int y)
{
    unsigned char negflag;
    unsigned char tempdegree;
    unsigned char comp;
    unsigned int degree;     // this will hold the result
    unsigned int ux;
    unsigned int uy;
    // Save the sign flags then remove signs and get XY as unsigned ints
    negflag = 0;
    if(x < 0)
    {
        negflag |= 0x01;    // x flag bit
        x = (0 - x);        // is now +
    }
    ux = x;                // copy to unsigned var before multiply
    if(y < 0)
    {
        negflag |= 0x02;    // y flag bit
        y = (0 - y);        // is now +
    }
    uy = y;                // copy to unsigned var before multiply

    // 1. Calc the scaled "degrees"
    if(ux > uy)
    {
        degree = (uy * 45) / ux;   // degree result will be 0-45 range
        negflag += 0x10;    // octant flag bit
    }
    else
    {
        degree = (ux * 45) / uy;   // degree result will be 0-45 range
    }

    // 2. Compensate for the 4 degree error curve
    comp = 0;
    tempdegree = degree;    // use an unsigned char for speed!
    if(tempdegree > 22)      // if top half of range
    {
        if(tempdegree <= 44) comp++;
        if(tempdegree <= 41) comp++;
        if(tempdegree <= 37) comp++;
        if(tempdegree <= 32) comp++;  // max is 4 degrees compensated
    }
    else     // else is lower half of range
    {
        if(tempdegree >= 2) comp++;
        if(tempdegree >= 6) comp++;
        if(tempdegree >= 10) comp++;
        if(tempdegree >= 15) comp++;  // max is 4 degrees compensated
    }
    degree += comp;   // degree is now accurate to +/- 1 degree!
    // Invert degree if it was X>Y octant, makes 0-45 into 90-45
    if(negflag & 0x10) degree = (90 - degree);
    // 3. Degree is now 0-90 range for this quadrant,
    // need to invert it for whichever quadrant it was in
    if(negflag & 0x02)   // if -Y
    {
        if(negflag & 0x01)   // if -Y -X
            degree = (180 + degree);
        else        // else is -Y +X
            degree = (180 - degree);
    }
    else     // else is +Y
    {
        if(negflag & 0x01)   // if +Y -X
            degree = (360 - degree);
    }
    return degree;
}
#endif // DEBUG

static const int16_t sin0_90_table[] =
{
    0,     572,   1144,  1715,  2286,  2856,  3425,  3993,  4560,  5126,  5690,  6252,  6813,  7371,  7927,  8481,
    9032,  9580,  10126, 10668, 11207, 11743, 12275, 12803, 13328, 13848, 14364, 14876, 15383, 15886, 16383, 16876,
    17364, 17846, 18323, 18794, 19260, 19720, 20173, 20621, 21062, 21497, 21925, 22347, 22762, 23170, 23571, 23964,
    24351, 24730, 25101, 25465, 25821, 26169, 26509, 26841, 27165, 27481, 27788, 28087, 28377, 28659, 28932, 29196,
    29451, 29697, 29934, 30162, 30381, 30591, 30791, 30982, 31163, 31335, 31498, 31650, 31794, 31927, 32051, 32165,
    32269, 32364, 32448, 32523, 32587, 32642, 32687, 32722, 32747, 32762, 32767
};

/**
 * Return with sinus of an angle
 * @param angle
 * @return sinus of 'angle'. sin(-90) = -32767, sin(90) = 32767
 */
int16_t sc_sin(int16_t angle)
{
    int16_t ret = 0;
    angle       = angle % 360;
    if(angle < 0) angle = 360 + angle;
    if(angle < 90)
    {
        ret = sin0_90_table[angle];
    }
    else if(angle >= 90 && angle < 180)
    {
        angle = 180 - angle;
        ret   = sin0_90_table[angle];
    }
    else if(angle >= 180 && angle < 270)
    {
        angle = angle - 180;
        ret   = -sin0_90_table[angle];
    }
    else     /*angle >=270*/
    {
        angle = 360 - angle;
        ret   = -sin0_90_table[angle];
    }
    return ret;
}

// Compute the fixed point square root of an integer and
// return the 8 MS bits of fractional part.
// Quicker than sqrt() for processors that do not have an FPU (e.g. RP2040)
uint8_t sc_sqrt(uint32_t num)
{
    if (num > (0x40000000)) return 0;
    uint32_t bsh = 0x00004000;
    uint32_t fpr = 0;
    uint32_t osh = 0;
    // Auto adjust from U8:8 up to U15:16
    while (num>bsh)
    {
        bsh <<= 2;
        osh++;
    }
    do
    {
        uint32_t bod = bsh + fpr;
        if(num >= bod)
        {
            num -= bod;
            fpr = bsh + bod;
        }
        num <<= 1;
    }
    while(bsh >>= 1);

    return fpr>>osh;
}

/********************************************
** Function name:   _sc_draw_Arc_line
** Description:     以线画圆提高效率
*******************************************x*/
void _sc_draw_Arc_line(int x,int y,uint16_t *buf, uint16_t len,char negflag)
{
    int i;
    if(negflag<0)
    {
        for(i=len-1; i>=0; i--)
        {
            gui->dma_prt[gui->dma_i++]=buf[i];
        }
        gui->Refresh(x-len+1,y,x,y); //横线加速
    }
    else
    {
        for( i=0; i<len; i++)
        {
            gui->dma_prt[gui->dma_i++]=buf[i];
        }
        gui->Refresh(x,y,x+len-1,y); //竖线加速
    }
}

void _Angle_to_endSlope(int startAngle, int endAngle,uint32_t *startSlope,uint32_t   *endSlope)
{
    uint32_t slope1,slope;
    int x,y;
    //---------startAngle斜率-----------------
    x = sc_sin(startAngle);
    y = sc_sin(startAngle+90);
    x=sc_abs(x);
    y=sc_abs(y);
    slope= (y<<16)/(x+1);
    //---------endAngle斜率-----------------
    x = sc_sin(endAngle);
    y = sc_sin(endAngle+90);
    x=sc_abs(x);
    y=sc_abs(y);
    slope1= (y<<16)/(x+1);

    if(startAngle<=90)
    {
        startSlope[0]=slope;            //显示
    }
    else if(startAngle<=180)
    {
        startSlope[0]=~startSlope[0];   //不显示
        startSlope[1]=slope;
    }
    else if(startAngle<=270)
    {
        startSlope[0]=~startSlope[0];    //不显示
        startSlope[1]=~startSlope[1];    //不显示
        startSlope[2]= slope;
    }
    else
    {
        startSlope[0]=~startSlope[0];
        startSlope[1]=~startSlope[1];
        startSlope[2]=~startSlope[2];
        startSlope[3]= slope;
    }
    //==========半圈用于旋转=================
    if(endAngle>450)
    {
        endSlope[1]=slope1;
    }
    else if(endAngle>360)
    {
        endSlope[0]=slope1;
    }
    else if(endAngle>270)
    {
        endSlope[3]=slope1;
    }
    else if(endAngle>180)
    {
        endSlope[2]=slope1;
        endSlope[3]=~endSlope[3];
    }
    else if(endAngle>90)
    {
        endSlope[1]=slope1;
        endSlope[2]=~endSlope[2];
        endSlope[3]=~endSlope[3];
    }
    else
    {
        endSlope[0]=slope1;
        endSlope[1]=~endSlope[1];
        endSlope[2]=~endSlope[2];
        endSlope[3]=~endSlope[3];
    }
}


/***************************************************************************************
** Function name:        SC_DrawArc1
** Description:          Draw an arc clockwise from 6 o'clock position
***************************************************************************************/

uint16_t  arc_line[4][100];    //缓存大小取决于半径

void _sc_div4_Arc(SC_arc *p,int w,int h,int16_t startAngle, int16_t endAngle,uint16_t fc,uint16_t bc)
{
    uint32_t slope;
    uint32_t startSlope[4]= {0xffffffff, 0, 0xffffffff, 0};  //全显条件
    uint32_t   endSlope[4]= {0, 0xffffffff, 0, 0xffffffff};

    uint16_t indx,a;
    uint16_t r2 =  p->r*p->r;
    uint16_t r3 = (p->ir)*(p->ir);
    uint16_t r  =  p->r+1;
    uint16_t rmax = (r)*(r);
    uint16_t rmin=(p->ir-1)*(p->ir-1);
    int x,y,st= 0;
    int dy2,temp;

    gui->colour[1]=bc;
    gui->colour[2]=fc;

    if(startAngle!=0||endAngle!=360)
    {
        _Angle_to_endSlope(startAngle,endAngle,startSlope,endSlope);
    }
    for ( y =r ; y>=0; y--)                 //1/4扇区计算
    {
        dy2 = y*y;
        while (p->ir&&(st)*( st) + dy2 < rmin) st++;//坐标判断点是否在圆环内
        indx=0;
        for (x=st; x <r; x++)
        {
            temp =x*x + dy2;
            if (temp>=rmax)  break ;        //大于rmax外圆跳过
            if (temp<r3)
            {
                a= sc_sqrt(temp);           //内边缘
            }
            else if (temp> r2)
            {
                a=(~sc_sqrt(temp))&0xff;           //外边缘
            }
            else
            {
                a=255;
            }
            if(gui->alpha<255)
            {
                bc= 0x0100;
                fc=  a*gui->alpha/256;
                if(gui->colour[1]) bc|=fc;
            }
            else
            {
                bc=alphaBlend(gui->colour[1],0,a);
                fc=alphaBlend(gui->colour[2],0,a);
            }
            slope =(x)>0?(y<<16)/x:0xfffffff;
            arc_line[0][indx]=bc;
            arc_line[1][indx]=bc;
            arc_line[2][indx]=bc;
            arc_line[3][indx]=bc;
            if (slope <  startSlope[0]&& slope>= endSlope[0])  arc_line[0][indx]=fc;
            if (slope >= startSlope[1]&& slope< endSlope[1])  arc_line[1][indx]=fc;
            if (slope <  startSlope[2]&& slope>=endSlope[2])  arc_line[2][indx]=fc;
            if (slope >= startSlope[3]&& slope< endSlope[3])  arc_line[3][indx]=fc;
            if(endAngle>360)
            {
                if(slope  < startSlope[0] ||slope >= endSlope[0])  arc_line[0][indx]=fc;
                if(endAngle>450)
                {
                    if (slope >= startSlope[1]|| slope< endSlope[1])   arc_line[1][indx]=fc;
                }
            }
            indx++;
        }
        if(p->s&0x01) _sc_draw_Arc_line(p->cx - st,p->cy + y +h, arc_line[0],indx,-1);     //BL
        if(p->s&0x02) _sc_draw_Arc_line(p->cx - st,p->cy - y,    arc_line[1],indx,-1);     //TL
        if(p->s&0x04) _sc_draw_Arc_line(p->cx + st+w,p->cy - y,    arc_line[2],indx,1);     //TR
        if(p->s&0x08) _sc_draw_Arc_line(p->cx + st+w,p->cy + y +h, arc_line[3],indx,1);     //BR
    }
    // SC_Angle_arc(p, startAngle, 1);
    // SC_Angle_arc(p, endAngle, -1);
}

/********************************************
** Function name:   SC_RoundFrame_AA
** Description:     画圆角矩形
*******************************************x*/
void SC_RoundFrame_AA(int x1,int y1,int x2,int y2,int r,int ir,uint16_t fc,uint16_t bc)
{
    SC_arc arc;
    int w=x2-x1-2*r;
    int h=y2-y1-2*r;
    arc.cx=x1+r;
    arc.cy=y1+r;
    arc.r=r;
    arc.ir=ir;
    arc.s=0xff;
    _sc_div4_Arc(&arc,w,h,0,360,fc,bc);
    w=r-ir;
    SC_FillFrame(x1+r+1,y1,x2-r-1,y1+w,fc);         //T
    SC_FillFrame(x1+r+1,y2-w,x2-r-1,y2,fc);         //B
    if(ir==0)
    {
        SC_FillFrame(x1,y1+r+1, x2,y2-r-1,fc);      //实芯
    }
    else
    {
        SC_FillFrame(x1,y1+r+1, x1+w,y2-r-1,fc);        //L
        SC_FillFrame(x2-w,y1+r+1, x2,y2-r-1,fc);        //R
    }
}

void SC_RoundFrame_half(int x1,int y1,int x2,int y2,int r,int ir,uint16_t fc,uint16_t bc)
{
    SC_arc arc;
    int w=x2-x1-2*r;
    int h=y2-y1-2*r;
    arc.cx=x1+r;
    arc.cy=y1+r;
    arc.r=r;
    arc.ir=ir;
    arc.s=0x0a;
    _sc_div4_Arc(&arc,w,h,0,360,fc,bc);
    w=r-ir;
    SC_FillFrame(x1+r+1,y1,x2,y1+w,fc);         //T
    SC_FillFrame(x1,y2-w,x2-r-1,y2,fc);         //B
    if(ir==0)
    {
        SC_FillFrame(x1,y1+r+1, x2,y2-r-1,fc);      //实芯
    }
    else
    {
        SC_FillFrame(x1,y1+r+1, x1+w,y2,fc);        //L
        SC_FillFrame(x2-w,y1, x2,y2-r-1,fc);        //R
    }
}
/***************************************************************************************
** Function name:        SC_DrawRound_AA
** Description:          画一个圆
***************************************************************************************/
void SC_DrawRound_AA(int cx,int cy,int r,int ir,uint16_t fc,uint16_t bc)
{
    SC_arc arc;
    arc.cx=cx+r;
    arc.cy=cy+r;
    arc.r=r;
    arc.ir=ir;
    arc.s=0xff;
    _sc_div4_Arc(&arc,0,0,0,360,fc,bc);
}


/***************************************************************************************
** Function name:        SC_DrawArc_AA
** Description:          画一个带角度圆弧
***************************************************************************************/
void SC_DrawArc_AA(SC_arc *p,int startAngle, int endAngle,uint16_t fc,uint16_t bc)
{
    p->s=0xff;
    _sc_div4_Arc(p,0,0,startAngle,endAngle,fc,bc);
    //sc_Angle_arc(p, startAngle, 1);
    // sc_Angle_arc(p, endAngle,  -1);
}
//圆弧读点函数
uint16_t sc_read_Arc(SC_arc *p,int x,int y,uint16_t bc)
{
    int16_t r=(p->r+1)*(p->r+1);
    int16_t ir=(p->r)*(p->r);
    int16_t pr = (y*y)+(x*x);
    if(pr<r)
    {
        if(pr>ir)
            return alphaBlend(bc,0,~sc_sqrt(pr));
        else
            return bc;
    }
    return 0;
}
//
////画弧角上圆头
//static void  SC_Angle_arc(SC_arc *p,int16_t Angle, char negflag)
//{
//    SC_arc temp;     //
//    uint8_t a;
//    int32_t x=0,y=0;
//    int32_t xend,yend,dy2=0,dx2=0;
//    int32_t  sin = -sc_sin(Angle);
//    int32_t  cos =  sc_sin(Angle+90);
//    int16_t rmin,rmax;
//
//    //-------------角度上小圆心座标-------------------
//    temp.r= (p->r-p->ir)>>1;
//    rmin=temp.r*temp.r;
//    rmax=(temp.r+1)*(temp.r+1);
//    if(sin<0)
//    {
//        temp.cx =(sin*(p->r-temp.r)-16348)/32768;  //圆心坐标4舍5入
//        sin=-sin;
//    }
//    else
//    {
//        temp.cx =(sin*(p->r-temp.r)+16348)/32768;
//    }
//    if(cos<0)
//    {
//        temp.cy = (cos*(p->r-temp.r)-16348)/32768;
//        cos=-cos;
//    }
//    else
//    {
//        temp.cy = (cos*(p->r-temp.r)+16348)/32768;
//    }
//    int xs = temp.cx-temp.r;
//    int xe = temp.cx+temp.r;
//    int ys = temp.cy-temp.r;
//    int ye = temp.cy+temp.r;
//    //printf("x=%d,y=%d Angle=%d\r\n",temp.cx,temp.cy,Angle);
//
//    if(Angle>360) Angle-=360;
//    if(Angle>135&&Angle<=315)  negflag*=-1;
//    if(sin>=cos)
//    {
//        yend=negflag>0?ye:ys;
//        for(x=xs; x<=xe; x++)
//        {
//            dx2=(x-temp.cx)*(x-temp.cx);
//            for(y=x*temp.cy/temp.cx-negflag;; y+=negflag)
//            {
//                dy2=(y-temp.cy)*(y-temp.cy)+dx2;
//                if(dy2<rmax)
//                {
//                    if(dy2>rmin)
//                    {
//                        a= ~sc_sqrt(dy2);
//                        uint16_t bc=sc_read_Arc(p,x,y,gui->colour[2]);
//                        gui->bsp_pset(p->cx+x, p->cy+y,alphaBlend(gui->colour[1],bc, a));
//                    }
//                    else
//                    {
//                        gui->bsp_pset(p->cx+x, p->cy+y,gui->colour[1]);
//                    }
//                }
//                if(y==yend) break;
//            }
//        }
//    }
//    else
//    {
//        xend=negflag>0?xe:xs;
//        for(y=ys; y<=ye; y++)
//        {
//            dy2=(y-temp.cy)*(y-temp.cy);
//            for(x=y*temp.cx/temp.cy-negflag;; x+=negflag)
//            {
//                dx2=(x-temp.cx)*(x-temp.cx)+dy2;
//                if(dx2<rmax)
//                {
//                    if(dx2>rmin)
//                    {
//                        a=~sc_sqrt(dx2);
//                        uint16_t bc=sc_read_Arc(p,x,y,gui->colour[2]);
//
//                        gui->bsp_pset(p->cx+x, p->cy+y,alphaBlend(gui->colour[1],bc, a));
//                    }
//                    else
//                    {
//                        gui->bsp_pset(p->cx+x, p->cy+y,gui->colour[1]);
//                    }
//                }
//                if(x==xend) break;
//            }
//        }
//    }
//}
//
////抗锯齿画直线，SDF算法圆头线
//#if 0
//float PixelAlphaGain   = 255.0;
//float LoAlphaTheshold  = 1.0/32.0;
//float HiAlphaTheshold  = 1.0 - 1.0/32.0;
//float capsuleSDF(float px, float py, float ax, float ay, float bx, float by, float r)
//{
//    float pax = px - ax, pay = py - ay, bax = bx - ax, bay = by - ay;
//    float h = fmaxf(fminf((pax * bax + pay * bay) / (bax * bax + bay * bay), 1.0f), 0.0f);
//    float dx = pax - bax * h, dy = pay - bay * h;
//    return sqrtf(dx * dx + dy * dy) - r;
//}
//void SC_DrawLine_SDF(float ax, float ay, float bx, float by, float r,u16 fc)
//{
//    if(ax<0||bx>LCD_SCREEN_WIDTH) return;
//    if(ay<0||by>LCD_SCREEN_HEIGHT) return;
//    if (r < 0.0) return;
//    int x0 = (int)floorf(fminf(ax, bx) - r);
//    int x1 = (int) ceilf(fmaxf(ax, bx) + r);
//    int y0 = (int)floorf(fminf(ay, by) - r);
//    int y1 = (int) ceilf(fmaxf(ay, by) + r);
//    float alpha=1.0;
//    bool endX;
//    int cnt=0;
//    for (int y = y0; y <= y1; y++)
//    {
//        endX = 0;                                       // Flag to skip pixels
//        for (int x = x0; x <= x1; x++)
//        {
//            alpha=0.5f - capsuleSDF(x, y, ax, ay, bx, by, r);
//            if (!endX)
//            {
//                if(y0>ay)
//                {
//                    endX = true;
//                    x0 = x;
//                }
//                if (alpha <= LoAlphaTheshold )continue;
//            }
//            else
//            {
//                if (alpha <= LoAlphaTheshold) break;   // Skip right side
//            }
//            uint16_t pcol =alphaBlend( fc, 0,(alpha * PixelAlphaGain));
//            gui->bsp_pset(x, y, pcol);
//        }
//    }
//}
//#endif
////SC快速抗锯齿画直线
//void SC_DrawLine_FXAA( int x1, int y1, int x2, int y2, u16 c)
//{
//    int  dx=0,dy=0,a,n;
//    int  dxabs = x2 - x1;
//    int  dyabs = y2 - y1;
//    int sgndx = (dxabs<0)?-1:1;
//    int sgndy = (dyabs<0)?-1:1;
//    dxabs = (dxabs>0)?dxabs:-dxabs;  //宽度
//    dyabs = (dyabs>0)?dyabs:-dyabs;  //高度
//    dx+=dyabs/2;   //+0.5
//    dy+=dxabs/2;   //+0.5
//    if(dxabs>dyabs)
//    {
//        for(n=0; n<=dxabs; n++) //横向 x>y
//        {
//            a=((dy%dxabs)<<8)/dxabs;
//            //==============插值========================
//            gui->bsp_pset(x1, y1-sgndy,alphaBlend(c,0, 255-a));
//            gui->bsp_pset(x1, y1,alphaBlend(c,0, 255));
//            gui->bsp_pset(x1, y1+sgndy,alphaBlend(c,0, a));
//            dy += dyabs;
//            if( dy >= dxabs )
//            {
//                dy -= dxabs;
//                y1 += sgndy;
//            }
//            x1+=sgndx;
//        }
//    }
//    else
//    {
//        for(n=0; n<=dyabs; n++)
//        {
//            a=((dx%dyabs)<<8)/dyabs;
//            //==============插值========================
//            gui->bsp_pset(x1-sgndx, y1,alphaBlend(c,0, 255-a));
//            gui->bsp_pset(x1, y1,alphaBlend(c,0, 255));
//            gui->bsp_pset(x1+sgndx, y1,alphaBlend(c,0, a));
//            dx += dxabs;
//            if( dx >= dyabs )
//            {
//                dx -= dyabs;
//                x1 += sgndx;
//            }
//            y1+= sgndy;
//        }
//    }
//}
