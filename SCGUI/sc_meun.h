
#ifndef _SC_MEUN_H_
#define	_SC_MEUN_H_

#include "sc_gui.h"

#include "sc_arc.h"
#include "sc_event_task.h"

struct sc_meun_t
{
    char *str;
    SC_img_t  *png;
    struct sc_meun_t *prev;
    void (*meun_key)(void *arg,u8 init);
};
typedef struct sc_meun_t sc_meun;

extern sc_meun *g_meun;

typedef struct topic_t
{
   uint8_t   answer;
   const char   *title;
   const char  *content[4];

}topic_t;


//----------------图片------------------------
extern const SC_img_t png_welcom; //LOGO
extern const SC_img_t png_home;   //首页

extern const SC_img_t png0;
extern const SC_img_t png1;
extern const SC_img_t png2;
extern const SC_img_t png3;
extern const SC_img_t png4;
extern const SC_img_t png5;

extern const SC_img_t png0_sel;   //年级选择
extern const SC_img_t png4_sel;   //难度选择

extern lv_font_t myFont_11;
extern lv_font_t myFont_12;
extern lv_font_t myFont_14;
extern lv_font_t myFont_12_4bpp;

#endif
