#ifndef SC_ARC_H
#define SC_ARC_H

typedef struct
{
    int16_t  cx;
    int16_t  cy;
    int16_t  r;
    int16_t  ir;
    uint8_t  s;
}SC_arc;

void SC_RoundFrame_AA(int x1,int y1,int x2,int y2,int r,int ir,uint16_t fc,uint16_t bc);
void SC_RoundFrame_half(int x1,int y1,int x2,int y2,int r,int ir,uint16_t fc,uint16_t bc);

void SC_DrawRound_AA(int cx,int cy,int r,int ir,uint16_t fc,uint16_t bc);

void SC_DrawArc_AA(SC_arc *p,int startAngle, int endAngle,uint16_t fc,uint16_t bc);


#endif

