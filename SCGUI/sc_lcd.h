#ifndef SC_LCD__H
#define SC_LCD__H

#include "lvgl.h"

#define LCD_SCREEN_WIDTH   320
#define LCD_SCREEN_HEIGHT  240


#define LCD_SCREEN_RAM_SIZE   (1024*3)   //3K




//#ifndef  NULL
//#define  NULL   0
//#endif
//
//typedef unsigned char   		u8, uint_8;
//typedef char            		s8;
//
//typedef unsigned short  		u16,uint_16;
//typedef signed 	 short    	    s16;
//
//typedef unsigned int    		u32,uint_32;
//typedef signed int      		s32;



void LCD_DMA_color(int xs, int ys, int xe, int ye, u16 *color);


#if  LCD_SCREEN_RAM_BUF
void SC_RAM_pset(int xs,int ys,u16 a);
u8   SC_RAM_read(int xs,int ys,u16 *c);
void SC_RAM_refresh(int xs,int ys,int xe,int ye);
int  SC_RAM_create(SC_RAM *ram,int xs,int ys,int xe,int ye,u16 c0,u16 c1,u16 c2,u16 c3);
void SC_RAM_free(SC_RAM *ram);
void SC_RAM_layou(SC_RAM *ram,u8 en);
void SC_Demo_box(void);
void SC_Demo_box_chart(u16 num);
#else

#define   SC_RAM_read(xs,ys,c)   (0)

#endif

#endif

