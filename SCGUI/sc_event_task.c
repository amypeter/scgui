
#include "sc_event_task.h"

sc_time_t  g_tick=0;					//系统时钟
sc_time_t  sc_tick=0;			 		//任务心跳
sc_task_t  sc_task[TASK_MAX]= {0};  	//任务表
static sc_event_t event;
static u8 t_in=0,t_out=0;			 	//队列变量

//创建事件线程，时间片最少为1ms
int sc_create_task(u8 id,void (*thread_cb)(void*),sc_time_t ms)
{
    sc_task[id].ms = ms;                 //线程定时
    sc_task[id].time_out = sc_tick+ms;
    if( sc_task[id].thread_cb!= thread_cb)
    {
        sc_task[id].thread_cb= thread_cb;
        return 1;
    }
    return 0;
}
//删除线程
void sc_delete_task(u8 id)
{
    if(	sc_task[id].thread_cb)
    {
        sc_task[id].thread_cb=NULL;
        sc_task[id].ms = 0;
    }
}
//线程存在否
u8 sc_task_true(u8 id)
{
    return sc_task[id].ms;
}
//遍历线程，
void sc_task_loop(void)
{
    void (*delay_cb) (void);
    static u8 id;                //静态防止堆栈不够
    event.key=0;
    if(g_tick!=sc_tick)
    {
        sc_tick++;				//g_tick为中断，sc_tick只作跟随保证for循环数据一致，
        if(t_in!=t_out)
        {
            event.key=sc_task[t_out].key;
            if(++t_out>=TASK_MAX) t_out = 0;  //全局事件出列
        }
        for(id=0; id<TASK_MAX; id++)
        {
            if(sc_task[id].delay_cb&&sc_task[id].time_dly==sc_tick)
            {
                delay_cb=sc_task[id].delay_cb;  //中间变量
                sc_task[id].delay_cb=NULL;
                delay_cb();                     //内部可以递归

            }
        }
    }
    for(id=0; id<TASK_MAX; id++)
    {
        event.s=sc_task[id].s;              //线程信号量
        if(sc_task[id].ms && sc_task[id].time_out==sc_tick)
        {
            event.ms=sc_task[id].ms;		//时间事件
            sc_task[id].thread_cb(&event);
            sc_task[id].time_out=sc_tick+sc_task[id].ms;
            sc_task[id].s=0;
        }
        else if(event.s||event.key)			//事件&信号量
        {
            if(sc_task[id].thread_cb)
            {
                event.ms=0;          		//清超时
                sc_task[id].thread_cb(&event);
                sc_task[id].s=0;
            }
        }
    }
}
//-----------------------------------------------------
//单次延时，可以递归使用
void sc_add_delay(u8 id,void (*delay_cb)(void),sc_time_t ms)
{
    sc_task[id].time_dly = sc_tick+ms;
    sc_task[id].delay_cb= delay_cb;
}
//单次延时，可以递归使用
void sc_stop_delay(u8 id)
{
    sc_task[id].time_dly = 0;
    sc_task[id].delay_cb= NULL;
}
//发送事件,全局队列
void sc_send_event(sc_sig_t key)
{
    sc_task[t_in].key=key;
    if(++t_in>=TASK_MAX)
    {
        t_in = 0;
    }
}
//发送信号量,指定线程
void sc_send_sig(u8 id, sc_sig_t sig)
{
    sc_task[id].s = sig;
}

/*
***********************************
*	函 数 名:按键扫描
***********************************/
void sc_key_scan(u16(*get_key)(void))
{
    u16 key=0;
    static  u8   cnt;         //双击
    static  u8   key_dly;     //多击间隔
    static	u16  key_count;
    static	u16  old_key;

    if(get_key==NULL)
    {
//        if(!gpio_read(POWER_WAKEUP_IO))
//        {
//            key|=KEY_EVENT_1;
//        }
        //if(!gpio_read(KEY_SW2)){key|=KEY_EVENT_2;}
    }
    else
    {
        key= get_key();
    }
    if(key)
    {
        key_count++;
        if(key_count==BSP_KEY_FILTER)
        {
            cnt++;
//            if(key==KEY_EVENT_1||key==KEY_EVENT_2)  //双击使能
//            {
//                key_dly=BSP_KEY_MULIT;
//            }
//            else
            {
                sc_send_event(key);                //按下
                key_dly=1;                       //单按
            }
        }
        if(key_count==BSP_KEY_LONG)
        {
            sc_send_event(key|KEY_LONG);           //长按事件
            key_dly=0;                           //无释放事件
        }
        if(key_count>=BSP_KEY_LONG+BSP_KEY_REPEAT)
        {
            key_count-=BSP_KEY_REPEAT;
            sc_send_event(key|KEY_LONG);          //长按连发
            key_dly=0;                          //无释放事件
        }
        old_key=key;
    }
    else
    {
        if(key_dly)
        {
            if(--key_dly==0||cnt==5)
            {
                sc_send_event(old_key|KEY_MULIT(cnt));    //释放事件
                key_dly=0;
            }
        }
        else
        {
            cnt=0;
            old_key=0;
        }
        key_count=0;
    }
}


