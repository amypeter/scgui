
#ifndef _SC_ENEVT_TASK_H_
#define	_SC_ENEVT_TASK_H_

#include "sc_gui.h"

#ifndef  NULL
#define	 NULL 0
#endif


typedef  uint32_t    sc_time_t;		//定义时间u16或u32
typedef  uint16_t    sc_sig_t;      //定义信号量u16或u32

typedef struct sc_event_t
{
    sc_sig_t       s;
    sc_sig_t       key;
    sc_time_t      ms;
} sc_event_t;

typedef struct sc_task_t
{
    sc_sig_t     s;
    sc_sig_t     key;
    void (*thread_cb) (void*);
    void (*delay_cb) (void);
    sc_time_t 	 time_out;
    sc_time_t 	 time_dly;
    sc_time_t 	 ms;
} sc_task_t;

enum
{
    TASK_ID_MAIN=0,
    TASK_ID_KEY,
    TASK_ID_2,
    TASK_ID_3,
    TASK_MAX,									//任务大小
};

extern  sc_time_t  g_tick;

//创建线程
int sc_create_task(u8 id,void (*thread_cb)(void*),sc_time_t ms);

//删除线程
void sc_delete_task(u8 id);

//遍历线程
void sc_task_loop(void);

u8 sc_task_true(u8 id);
//单次延时
void sc_add_delay(u8 id,void (*delay_cb)(void),sc_time_t ms);
//发送信号实时,指定ID通信
void sc_send_sig(u8 id, sc_sig_t sig);

//发送事件队列,全局通信
void sc_send_event(sc_sig_t key);
//--------------------------------------------
//信号量定义

#define SIG_INIT        (0x01)  //
#define SIG_BAT_LOW     (0x02)
#define SIG_BAT_SLEEP   (0x04)

//按键事件定义
#define KEY_EVENT_UP    (0x01)
#define KEY_EVENT_DP    (0x02)
#define KEY_EVENT_L    (0x04)
#define KEY_EVENT_R    (0x08)

#define KEY_EVENT_OK    (0x10)
#define KEY_EVENT_BLACK (0x20)

#define KEY_EVENT_VOLN  (0x40)
#define KEY_EVENT_VOLP  (0x80)


#define KEY_LONG       (1<<15)//长按
#define KEY_REL        (1<<8) //释放
#define KEY_MULIT(n)   (n<<8) //多击

#define KEY_1_REL    (KEY_1|KEY_REL)       //释放
#define KEY_1_LONG   (KEY_1|KEY_LONG)      //长按
#define KEY_1_MULIT  (KEY_1|KEY_MULIT(3))  //三连按
#define KEY1_KEY2    (KEY_1|KEY_2)         //12组合

//按键事件驱动
#define BSP_KEY_FILTER   3           /*消抖计数*/
#define BSP_KEY_LONG     100         /*长按定义*/
#define BSP_KEY_MULIT    20			 /*多击间隔*/
#define BSP_KEY_REPEAT   50			 /*长按连发速度*/

void sc_key_scan(u16(*get_key)(void));

#endif

