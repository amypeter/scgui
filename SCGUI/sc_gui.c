
/*---------------------------------------------------------------
*                        SC_GUI for V1.1
* 一个小内存(1K)占用的开源GUI,支持背景透明度,支持LVGL抗锯齿字体,动态文字,实用波形图
* 感谢网友开源工具Lvgl Font Tool V0.4 生成 阿里(qq:617622104)
* 作者:黎R(WX:13018616872)
* 社区:(QQ群:799501887)
*---------------------------------------------------------------*/
#include "sc_gui.h"

SC_GUI  *gui;
u16 DMA_buf_color[2][480] = {0};  //DMA内存屏幕大小
//DMA连续读点
int Dma_read(int x,int y,u16 *c)
{
    return 0;
}
//DMA连续画点
void Dma_put(int x,int y,u16 a)
{
    gui->dma_prt[gui->dma_i++]=a;
}
//快速运算alpha算法
uint16_t alphaBlend( uint16_t fgc, uint16_t bgc,uint8_t alpha)
{
    if(alpha<16)   return bgc;
    if(alpha>240)  return fgc;
    // Split out and blend 5 bit red and blue channels
    uint32_t rxb = bgc & 0xF81F;
    rxb += ((fgc & 0xF81F) - rxb) * (alpha >> 2) >> 6;
    // Split out and blend 6 bit green channel
    uint32_t xgx = bgc & 0x07E0;
    xgx += ((fgc & 0x07E0) - xgx) * alpha >> 8;
    // Recombine channels
    return (rxb & 0xF81F) | (xgx & 0x07E0);
}
//快速运算alpha算法,静态变量优化减少重复运算
//static u32 bc_Alpha,fc_Alpha,c_sum;
//u32 SC_AlphaBlender(u16 fc,u16 bc,int Alpha)
//{
//    u32  result;
//    if(Alpha<16)   return bc;
//    if(Alpha>240)  return fc;
//    result=Alpha>>3;            //0-31级，大了会溢出
//    if(c_sum!=(fc-bc))
//    {
//        c_sum=fc-bc;
//        bc_Alpha = ( bc |( bc<<16 ) ) & 0x7E0F81F;
//        fc_Alpha = ( fc |( fc<<16 ) ) & 0x7E0F81F;
//    }
//    result=((fc_Alpha - bc_Alpha) * result)>>5;
//    result  = (result+bc_Alpha)& 0x7E0F81F;
//    return (u32)(result&0xFFFF) | (result>>16);
//}

//DMA区域读点混色
void  Dma_read_alpha(int xs,int ys,int xe,int ye,SC_img_t *gbkg,u16 *buf)
{
    u16 *map;
    u16 bc;
    u16 i,len=ye-ys,n=1;
    if(gbkg)             //取背景图
    {
        map=(u16*)gbkg->map;
        map+=gbkg->w*ys+xs;
        n=gbkg->w;
    }
    if((xe-xs)>len)
    {
        len=xe-xs;
        n= 1;
    }
    //-----------------------线缓存混色-----------------------
    for(i=0; i<=len; i++)
    {
        bc=gui->colour[0];
        if(gbkg)
        {
            bc=*map;
            map+=n;
        }
        if(buf[i]&0x0100)               //图形背景
        {
            buf[i]=alphaBlend(gui->colour[1],bc,buf[i]);
        }
        else if(buf[i]&0x0200)          //字体背景
        {
            if(gui->colour[1]!=0)
            {
                bc=alphaBlend(gui->colour[1],bc,gui->alpha);
            }
            buf[i]=alphaBlend(gui->colour[2],bc,buf[i]);
        }
        else
        {
            buf[i]=alphaBlend(gui->colour[2],bc,buf[i]);
        }
    }
}


//DMA_双buf刷新屏幕
void Dma_Refresh(int xs, int ys,int xe, int ye)
{
    if(gui->dma_i==0)  return;
    if(gui->alpha<255)
    {
        Dma_read_alpha(xs, ys,xe, ye,gui->gbkg,gui->dma_prt);
    }
    LCD_DMA_color(xs, ys,xe, ye,gui->dma_prt);
    //-------------- 双BUF切换------------------------------
    if(gui->dma_prt==gui->dma_buf1)
    {
        gui->dma_prt=gui->dma_buf2;
    }
    else
    {
        gui->dma_prt=gui->dma_buf1;
    }
    gui->dma_i = 0;
}

//初始化系统注册画点函数
void SC_GUI_Init( void (*bsp_pset)(int,int,u16),u16 bc,u16 c1,u16 c2,u16 c3, void* font)
{
    static SC_GUI tft;
    gui = &tft;
    gui->colour[0]=bc;
    gui->colour[1]=c1;
    gui->colour[2]=c2;
    gui->colour[3]=c3;
    gui->alpha=255;
    gui->state=0;
    gui->lcd_area.xs=0;
    gui->lcd_area.ys=0;
    gui->lcd_area.xe=LCD_SCREEN_WIDTH-1;
    gui->lcd_area.ye=LCD_SCREEN_HEIGHT-1;
    gui->align=&gui->lcd_area;
    gui->gbkg=NULL;
    gui->font=font;                   //GUI字库

    gui->dma_prt= (u16*)DMA_buf_color[0];
    gui->dma_buf1=(u16*)DMA_buf_color[0];
    gui->dma_buf2=(u16*)DMA_buf_color[1];

    gui->dma_i= 0;
    gui->Refresh=Dma_Refresh;
    gui->bsp_pset = bsp_pset;         //层底画点
}
//显示图片，DMA加速
void SC_Show_Image(int xs,int ys,SC_img_t *bmp,u8 load)
{
    int x,y;
    u16 *map=(u16*)bmp->map;
    uint8_t bak_alpha=gui->alpha;
    if(load) gui->alpha=255;
    for(y=0; y<bmp->h; y++)
    {
        for(x=0; x<bmp->w; x++)
        {
            Dma_put(x,y,map[x]);  //先复制到内存再打开DMA
        }
        gui->Refresh(xs, y+ys,x-1,y+ys);
        map+=bmp->w;
    }
    if(load)
    {
        gui->alpha=bak_alpha;
        gui->gbkg=bmp;            //用作背景
    }
}

void SC_FillFrame(int xs,int ys,int xe,int ye,u16 fc)
{
    int x,y;
    gui->colour[2] =  fc;
    if(gui->alpha<255) fc=gui->alpha;
    if((xe-xs)>(ye-ys))
    {
        for(y=ys; y<=ye; y++)
        {
            for(x=xs; x<=xe; x++)
            {
                gui->dma_prt[gui->dma_i++]=fc;   //传送的是透明度
            }
            gui->Refresh(xs, y,xe, y);           //横线加速
        }
    }
    else
    {
        for(x=xs; x<=xe; x++)
        {
            for(y=ys; y<=ye; y++)
            {
                gui->dma_prt[gui->dma_i++]=fc;
            }
            gui->Refresh(x, ys,x,ye); //竖线加速
        }
    }
}
void SC_DrawFrame( int xs, int ys, int xe, int ye,u16 fc)
{
    SC_FillFrame( xs, ys, xs, ye,fc);
    SC_FillFrame( xe, ys, xe, ye,fc);
    SC_FillFrame( xs, ys, xe, ys,fc);
    SC_FillFrame( xs, ye, xe, ye,fc);
}

///======================SCGUI字体=====================================
/* 取模设置，阴码，顺向C51取模从左到右
* width：字模宽
* higth: 字模高*/
void SC_Show_png(int xs,int ys,u16 fc, u16 bc,u16 width,u16 higth, u8 *buf,u8 bpp)
{
    int x,y;
    u8 a,i=0;
    // u8 d=~(0xff>>bpp);
    u8 size=(width*bpp+7)/8;            //取位图宽度
    u8 *map;
    for(x=xs; width--; x++)            //纵向解析效率更高
    {
        if(x<=gui->align->xs||x>=gui->align->xe)
        {
            continue;
        }
        map=buf+(x-xs)/8;                //
        for(y=ys; y<ys+higth; y++)
        {
            i=(7-(x-xs)%8);
            a=(map[0]>>i)&0x01;
            if(a)
            {
                Dma_put(x,y,fc);
            }
            else
            {
                Dma_put(x,y,bc);
            }
            map+=size;                  //纵向下一列
        }
        gui->Refresh(x, ys,x, y-1);     //纵向输出
    }
}
//设置文本对齐
void SC_text_align(SC_AERA* p,u16 state)
{
    gui->state&=~0x07;
    gui->align=&gui->lcd_area;
    if(p)
    {
        gui->state|=state;
        gui->align=p;
    }
}
//显示字符串中英混合
//code保留格式
void SC_Show_str1(int xs,int ys,u16 fc,u16 bc,const char* str, u16 code,void* sc_font)
{
    u16 k,j=0;
    SC_FONT  *font;
    char *gbk;
    u8 *map[20];          //指向位图20个居中字符
    u8 flag[20];          //中英标志
    int xlen=0;
    while(*str)
    {
        font=sc_font;
        if((*str<='~')&&(*str>=' '))        //判断是不是A符!
        {
            k=(*str)-' ';                   //全字库
            flag[j]=0;
            map[j++]=&font->Tab_buf[k* font->Tab_len];
            xlen+=font->w;
            str++;
        }
        else
        {
            if(font->List)   //查表特殊字符
            {
                for (k=0; font->List[k]; k++)
                {
                    gbk=&font->List[k*2];
                    if (gbk[0]==str[0]&&gbk[1]==str[1])
                    {
                        flag[j]=0;
                        map[j++]=&font->Tab_buf[(95+k)* font->Tab_len];
                        xlen+=font->w;
                        k=0xff;
                        break;
                    }
                }
            }
            if(k!=0xff)
            {
                font=sc_font+1;
                if(font->List)   //查表中文
                {
                    for (k=0; font->List[k]; k++)
                    {
                        gbk=&font->List[k*2];
                        if (gbk[0]==str[0]&&gbk[1]==str[1])
                        {
                            flag[j]=1;
                            map[j++]=&font->Tab_buf[k* font->Tab_len];
                            xlen+=font->w;
                            break;
                        }
                    }
                }
            }
            str+=2;
        }
    }
    if(gui->state&SC_STATE_STR_MID)          //0x03
    {
        if(gui->state&SC_STATE_STR_RIGHT)    //0x02
        {
            xlen=(gui->align->xe- xlen);
            xs+=(gui->state&SC_STATE_STR_LEFT) ? (xlen+gui->align->xs)/2:xlen;
        }
        else
        {
            xs+=gui->align->xs;        //0x01
        }
        ys+=gui->align->ys;
    }
    for(k=0; k<j; k++)
    {
        font=sc_font+flag[k];
        SC_Show_png(xs,ys+font->offsy,fc,bc,font->w,font->h,map[k],1);
        xs+=font->w;
    }
}
#if 1
///======================SCGUI LVGL字体=====================================

//将utf-8编码转为unicode编码（函数来自LVGL）
static uint32_t lv_txt_utf8_next(const char * txt, uint32_t * i)
{
    uint32_t result = 0;
    uint32_t i_tmp = 0;
    if(i == NULL) i = &i_tmp;
    if((txt[*i] & 0x80) == 0)     //Normal ASCII
    {
        result = txt[*i];
        (*i)++;
    }
    else    //Real UTF-8 decode
    {
        // bytes UTF-8 code
        if((txt[*i] & 0xE0) == 0xC0)
        {
            result = (uint32_t)(txt[*i] & 0x1F) << 6;
            (*i)++;
            if((txt[*i] & 0xC0) != 0x80) return 0; //Invalid UTF-8 code
            result += (txt[*i] & 0x3F);
            (*i)++;
        }
        //3 bytes UTF-8 code
        else if((txt[*i] & 0xF0) == 0xE0)
        {
            result = (uint32_t)(txt[*i] & 0x0F) << 12;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += (uint32_t)(txt[*i] & 0x3F) << 6;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += (txt[*i] & 0x3F);
            (*i)++;
        }

        else if((txt[*i] & 0xF8) == 0xF0)
        {
            result = (uint32_t)(txt[*i] & 0x07) << 18;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += (uint32_t)(txt[*i] & 0x3F) << 12;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += (uint32_t)(txt[*i] & 0x3F) << 6;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += txt[*i] & 0x3F;
            (*i)++;
        }
        else
        {
            (*i)++;
        }
    }
    return result;
}
//LVGL抗锯齿显示单字,图标
void lv_draw_letter(int xs, int ys,lv_font_glyph_dsc_t *gdsc,uint32_t unicode,lv_font_t * font,uint16_t fc,uint16_t bc)
{
    const u8 *buf;
    int x,y,ye=ys+font->line_height;
    u16 higth,width;
    u8 a,d=~(0xff>>gdsc->bpp);
    u8 size,i=0;

    width=gdsc->adv_w;
    higth=ye-gdsc->box_h-(gdsc->ofs_y+font->base_line);
    size=(width*gdsc->bpp+7)/8;                             //取位图宽度
    buf =font->get_glyph_bitmap(font,unicode);

    gui->colour[1]=bc;
    gui->colour[2]=fc;
    for(x=xs; width--; x++)
    {
        if(x<=gui->align->xs||x>=gui->align->xe)
        {
            continue;
        }
        u8 *map=(u8*)buf;
        for(y=ys; y<ye; y++)//gdsc->box_h
        {
            if(y<higth||y>=ye-(gdsc->ofs_y+font->base_line))  //空白区
            {
                a=0;
            }
            else
            {
                a= map[0]<<i;
                a=(a&d)?a|(~d):0;
                map+=size;                               //纵向下一列
            }
            if(gui->alpha<255)
            {
                fc= a;
                Dma_put(x,y,fc|0x0200);
            }
            else
            {
                fc=alphaBlend(gui->colour[2],gui->colour[1],a);
                Dma_put(x,y,fc);
            }
        }
        i+=gdsc->bpp;
        if(i>=8)
        {
            i=0;
            buf++;
        }
        gui->Refresh(x, ys,x, ye-1);           //纵向输出
    }

}
void SC_Show_str(int x,int y,u16 fc,u16 bc,const char* txt, u16 code,void* lv_font)
{
    lv_font_t* font=(lv_font_t*)lv_font;
    lv_font_glyph_dsc_t g[30];    //lv_font_glyph_dsc_t g;  //描述符号的属性。
    uint32_t unicode[30];         //最大字符数
    uint32_t i=0,j=0,k;

    int xs=x;
    int ys=y;
    int xlen=0;
    while(j<30)
    {
        unicode[j] =code>1?code: lv_txt_utf8_next(txt,&i);        //txt转unicode
        code=0;
       // printf(" unicode[j]=%d\r\n", unicode[j]);
        if(font->get_glyph_dsc(font,&g[j],unicode[j],0)==0||(unicode[j]==0x0d)||(unicode[j]==0x0a))      //结束
        {
            if(gui->state&SC_STATE_STR_MID)  //0x03
            {
                if(gui->state&SC_STATE_STR_RIGHT)    //0x02
                {
                    xlen=(gui->align->xe- xlen);
                    x+=(gui->state&SC_STATE_STR_LEFT) ? (xlen+gui->align->xs)/2:xlen;
                }
                else
                {
                    x+=gui->align->xs;        //0x01
                }
                y+=gui->align->ys;
            }
            for(k=0; k<j; k++)
            {
                lv_draw_letter(x,y,&g[k],unicode[k],font,fc,bc);
                x+=g[k].adv_w;
            }
            if((unicode[j]==0x0d)||(unicode[j]==0x0a))
            {
                ys+=font->line_height;
                x=xs;
                y=ys;
                j=0;
                xlen=0;
                continue;
            }
            break;
        }
        xlen+=g[j++].adv_w;
    }

}
#endif

//显示数字
//num=123,div=0  显示0123
//num=123,div=10 显示12.3
void SC_Show_float(int x,int y,u16 fc,u16 bc,int num,u16 div,void* lv_font)
{
    char num_str[10];
    char *p=num_str;
    if(num<0)
    {
        num*=-1;
        *p++='-';                   //-号
    }
    for(u16 temp=10000; temp; temp/=10)
    {
        if(num>=temp||div>=temp)
        {
            *p++=num/temp%10+'0'; //
        }
        else
        {
            //if(temp<10000)    //int型不够4位补0
            *p++='0';
        }
        if(div&&temp==div)
        {
            *p++='.';           //float小数点
        }
    }
    *p='\0';
    SC_Show_str1(x,y,fc,bc,num_str, 0,lv_font);
}
///======================SCGUI 波形图=====================================
//波形线性插值，采样点数放大到全屏
void SC_bilinear_bmp(int *src,int *dst,u32 src_width, u32 dst_width)
{
    u32  j,x;
    s32  result;
    s32  x_diff, x_diff_comp;           //权重
    for ( j = 0; j < dst_width; j++)
    {
        u32  x_ratio = (j<<8)*(src_width-1) / (dst_width-1); //缩放系数,防止边界问题指针超出
        x =  x_ratio >> 8;                                   //y坐标取整
        x_diff = x_ratio - (x << 8);                         //坐标取余
        x_diff_comp = (1 << 8) - x_diff;                     //1-余
        //-----------邻值取权重--------------
        result = x_diff_comp*src[x];        //x,y;
        result+= x_diff *src[x + 1];        //x+1,y;
        dst[j]=result>>8;
    }
}

#define CHART_BUF_PIX     60     //采样点
int chart_buf[CHART_BUF_PIX];
//采样点移动
void SC_chart_put_dat(int dat)
{
    u16 i;
    for(i=1; i<CHART_BUF_PIX; i++)
    {
        chart_buf[i-1]=chart_buf[i];     //波形前移
    }
    chart_buf[i-1]=dat;                  //入队列
}

void  SC_Show_chart(SC_AERA* chart,int xd,int yd,int vol,u16 fc,u16 gc)
{
    static u8 xtime=0;
    int vol_last;
    int x,y,i=0;
    u16  colour;
    int  out_buf[240];
    u16 width= chart->xe-chart->xs+1;
    u16 higth= chart->ye-chart->ys+1;
    xd=width/xd;
    yd=higth/yd;
    SC_chart_put_dat(vol);
    SC_bilinear_bmp(chart_buf,out_buf,CHART_BUF_PIX,width);//对波形进行插值放大
    vol_last=out_buf[i];
    xtime++;             //时间轴
    fc=gui->colour[fc];			     //波形
    gc=gui->colour[gc];			     //波形
    gui->state|=SC_STATE_IMAGE;
    for(x=0; x<width; x++)
    {
        int min=higth-out_buf[i];
        int max=higth-vol_last;
        if(min>max)
        {
            min=max;
            max=higth-out_buf[i];
        }
        for(y=0; y<higth; y++)
        {
            if(SC_RAM_read(chart->xs+x,chart->ys+y,&colour)>0)
            {
                //读点
            }
            else if(y>=min&&y<=max) //max无限大就是实心波形
            {
                colour=fc;			//波形
            }
            else
            {
                if(y%yd==0||x==width-1||x==0)
                {
                    colour=gc;
                }
                else   if((x+xtime)%xd==0&& y&0x01)
                {
                    colour=gc;
                }
                else
                {
                    colour=gui->colour[0];
                }
            }
            Dma_put(chart->xs+x,chart->ys+y,colour);	     //
        }
        gui->Refresh(chart->xs+x, chart->ys,chart->xs+x, chart->ye);
        vol_last=out_buf[i++];
    }
    gui->state&=~SC_STATE_IMAGE;
}

//void  SC_AERA_test_box(void)
//{
//    static  SC_AERA box1;
//
//    SC_AERA_cfg_box(&box1,90,50,320-91,120,C_YELLOW,C_MEDIUM_PURPLE);
//    SC_AERA_Show_box(&box1,64,4,"box1",C_MEDIUM_PURPLE,3);
//    // SC_Show_str1(box1.xs+80,box1.ys+40,C_YELLOW,"8",0,gui->font);
//}

//
//
////检查坐标区域内
//uint8_t sc_mask_area(SC_arc *p,int x,int y)
//{
//    if(x<p->xs||x>p->xe) return 0;
//    if(y<p->ys||y>p->ye) return 0;
//    return 255;
//}
////检查坐标区域内
//uint8_t sc_mask_arc(SC_arc *p,int x,int y)
//{
//    int16_t r=(p->r+1)*(p->r+1);
//    int16_t ir=(p->r)*(p->r);
//    int16_t  cx=x-p->cx;
//    int16_t  cy=y-p->cy;
//    cy = (cy*cy)+(cx*cx);
//    if(cy<r)
//    {
//        if(cy>ir)
//            return ~sc_sqrt(cy);
//        else
//            return 255;
//    }
//    return 0;
//}
////计算两个端点圆,
//uint8_t capsule_arc2(int px, int py, int xs, int ys, int xe, int ye, int r,int ir)
//{
//    uint8_t a=0;
//    int   sxy,exy;
//    r=r;
//    ir=ir;
//    sxy = (px - xs)*(px - xs)+(py - ys)*(py - ys);//开始的圆
//    if(sxy<r)
//    {
//        if(sxy>ir)
//            a=  ~sc_sqrt(sxy);
//        else
//            a=255;
//    }
//    exy = (px - xe)*(px - xe)+ (py - ye)*(py - ye);//结束的圆
//    if(exy<r)
//    {
//        if(a>128) return 255;   //两个圆重叠
//        if(exy>ir)
//            a=  ~sc_sqrt(exy);
//        else
//            a= 255;
//    }
//    return a;
//}
//
////测试画两个圆
//void capsule_arc2_test(void)
//{
//    uint16_t fc=gui->colour[1];
//    uint8_t a;
//    int r=20*20;
//    int ir=19*19;
//    for(int x=0; x<100; x++)
//    {
//        for(int y=0; y<100; y++)
//        {
//            a=capsule_arc2( x,  y,  58,  58,  80,  80,  r, ir);
//            if(a>16)
//                gui->bsp_pset(  x, y,alphaBlend(fc,0,a));
//        }
//    }
//}
