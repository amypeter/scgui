
//#include <iostream>
#include "sdl_lcd.h"
#include "sc_gui.h"
SDL_Window* gWindow = NULL;
SDL_Surface* gScreenSurface = NULL;
SDL_Surface* gHelloWorld = NULL;
SDL_Renderer* gRenderer = NULL;

int init_SDL2(void)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL can not initialized!SDL Error:%s\n", SDL_GetError());
        return 0;
    }
    //放大像素
    gWindow = SDL_CreateWindow("SC_LCD", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, LCD_SCREEN_WIDTH*2, LCD_SCREEN_HEIGHT*2, SDL_WINDOW_SHOWN);
    if (gWindow == NULL)
    {
        printf("SDL can't create window!SDL Error:%s\n", SDL_GetError());
        return -1;
    }
    gScreenSurface = SDL_GetWindowSurface(gWindow);

    gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
    if (gRenderer == NULL)
    {
        return 0;
    }
    // SDL_FillRect(gScreenSurface, NULL, SDL_MapRGBA(gScreenSurface->format, 0, 0, 0, 0));
    //----------------SDL2---------------------------------//
    SDL_RenderClear(gRenderer);
    SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xff);     // bg
    printf("open WindowShow \n");
    return 1;
}

void close_SDL2(void)
{
    SDL_FreeSurface(gHelloWorld);
    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(gWindow);
    SDL_Quit();
}

//GB565 画一个点
void SDL_DrawPoint ( int16_t x, int16_t y, uint16_t c )
{
    Uint8 r,g,b;
    if(x>=LCD_SCREEN_WIDTH) return;				//不换行
    /* Convert RGB565 to RGB888 */
    r = (c>>11)&0x1F;
    r<<=3;
    g = (c>>5)&0x3F;
    g<<=2;
    b = (c)&0x1F;
    b<<=3;
    SDL_SetRenderDrawColor(gRenderer,(Uint8)r, (Uint8)g, (Uint8)b, 0xff);
#if 1
    SDL_RenderDrawPoint(gRenderer, 2*x,2*y);        //放大像素
    SDL_RenderDrawPoint(gRenderer, 2*x+1,2*y);
    SDL_RenderDrawPoint(gRenderer, 2*x,2*y+1);
    SDL_RenderDrawPoint(gRenderer, 2*x+1,2*y+1);
#else
    SDL_RenderDrawPoint(gRenderer, x,y);        //1:1像素
#endif // 1

}
//---------------------------API---------------------------------------------
int get_SDL_Event(void)       //键盘事件
{
    char key=0;
    SDL_Event evt;
    while (SDL_PollEvent(&evt) != 0)
    {
        //event.type存储了当前的事件类型
        //如果无键盘鼠标 触摸点击 那么 默认是 0x200
        switch (evt.type)
        {
        case SDL_KEYDOWN:

            printf("\nk=%s",  SDL_GetKeyName(evt.key.keysym.sym));
            const Uint8 *state = SDL_GetKeyboardState(NULL);

            if ( state[SDL_SCANCODE_UP])   key='U';
            if ( state[SDL_SCANCODE_DOWN] )key='D';
            if ( state[SDL_SCANCODE_LEFT] )key='L';
            if ( state[SDL_SCANCODE_RIGHT])key='R';

            if ( state[SDL_SCANCODE_0] )key='0';
            if ( state[SDL_SCANCODE_1] )key='1';
            if ( state[SDL_SCANCODE_2] )key='2';
            if ( state[SDL_SCANCODE_3] )key='3';
            if ( state[SDL_SCANCODE_4] )key='4';
            if ( state[SDL_SCANCODE_5] )key='5';
            if ( state[SDL_SCANCODE_6] )key='6';
            if ( state[SDL_SCANCODE_7] )key='7';
            if ( state[SDL_SCANCODE_8] )key='8';
            if ( state[SDL_SCANCODE_9] )key='9';

            if ( state[SDL_SCANCODE_A]) key='~';
            if ( state[SDL_SCANCODE_S]) key='+';
            if ( state[SDL_SCANCODE_D]) key='-';
            if ( state[SDL_SCANCODE_F]) key='*';
            if ( state[SDL_SCANCODE_G]) key='/';

            if ( state[SDL_SCANCODE_PAGEDOWN])  key='O';  //回车
            if ( state[SDL_SCANCODE_PAGEUP])    key='B';  //清除
            if ( state[SDL_SCANCODE_DELETE])    key='d';  //删除
        // Menu_Put_Event(k);
        case SDL_KEYUP:

            break;
        case SDL_TEXTINPUT:
            //如果是文本输入 输出文本
            // PrintText(event.text.text);
            break;
        //case SDL_MOUSEBUTTONDOWN:
        /* 如果有任何鼠标点击消息 或者SDL_QUIT消息 那么将退出窗口 */
        case SDL_QUIT:
            return -1;
            break;
        default:
            break;
        }
    }
    return key;
}

void up_lcd(void)
{
    SDL_RenderPresent(gRenderer);           //更新
}


