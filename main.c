
#include "sdl_lcd.h"
#include "sc_gui.h"
#include "sc_meun.h"

int tick_thread(void * data);

int get_sdl2_key(void)
{
    int key=0;
    SDL_Event evt;
    while (SDL_PollEvent(&evt) != 0)
    {
        //event.type存储了当前的事件类型
        //如果无键盘鼠标 触摸点击 那么 默认是 0x200
        switch (evt.type)
        {
        case SDL_KEYDOWN:

            printf("\nk=%s",  SDL_GetKeyName(evt.key.keysym.sym));
            const Uint8 *state = SDL_GetKeyboardState(NULL);

            if ( state[SDL_SCANCODE_UP])        sc_send_event(KEY_EVENT_UP);
            if ( state[SDL_SCANCODE_DOWN] )     sc_send_event(KEY_EVENT_DP);
            if ( state[SDL_SCANCODE_LEFT] )     sc_send_event(KEY_EVENT_L);
            if ( state[SDL_SCANCODE_RIGHT])     sc_send_event(KEY_EVENT_R);
            if ( state[SDL_SCANCODE_BACKSPACE]) sc_send_event(KEY_EVENT_BLACK);//返回
            if ( state[SDL_SCANCODE_RETURN])    sc_send_event(KEY_EVENT_OK);//回车

            if ( state[SDL_SCANCODE_0] )key='0';
            if ( state[SDL_SCANCODE_1] )key='1';
            if ( state[SDL_SCANCODE_2] )key='2';
            if ( state[SDL_SCANCODE_3] )key='3';
            if ( state[SDL_SCANCODE_4] )key='4';
            if ( state[SDL_SCANCODE_5] )key='5';
            if ( state[SDL_SCANCODE_6] )key='6';
            if ( state[SDL_SCANCODE_7] )key='7';
            if ( state[SDL_SCANCODE_8] )key='8';
            if ( state[SDL_SCANCODE_9] )key='9';

            if ( state[SDL_SCANCODE_A]) key='~';
            if ( state[SDL_SCANCODE_S]) key='+';
            if ( state[SDL_SCANCODE_D]) key='-';
            if ( state[SDL_SCANCODE_F]) key='*';
            if ( state[SDL_SCANCODE_G]) key='/';

            if ( state[SDL_SCANCODE_PAGEDOWN])   key='O';
            if ( state[SDL_SCANCODE_PAGEUP])     key='B';
            if ( state[SDL_SCANCODE_DELETE])    key='d';  //删除
        // Menu_Put_Event(k);
        case SDL_KEYUP:

            break;
        case SDL_TEXTINPUT:
            //如果是文本输入 输出文本
            // PrintText(event.text.text);
            break;
        //case SDL_MOUSEBUTTONDOWN:
        /* 如果有任何鼠标点击消息 或者SDL_QUIT消息 那么将退出窗口 */
        case SDL_QUIT:
            return -1;
            break;
        default:
            break;
        }
    }
//   if(key)  sc_send_event(key);
    return 0;
}


void task_main_cb(void *arg)
{
    sc_event_t *e=arg;
    //---------私有菜单事件------------
    g_meun->meun_key(arg,e->s&SIG_INIT);
    //---------菜单公共事件-------------
    if(e->key==KEY_EVENT_BLACK)   //返回
    {
        if(g_meun->prev)
        {
            g_meun=g_meun->prev;
            g_meun->meun_key(e,1);
        }
    }
    if(e->key==KEY_EVENT_VOLN){}  //音量加
    if(e->key==KEY_EVENT_VOLP){}  //音量减
    //-----------定时器-------------------
    if(e->ms)
    {
      //sc_key_scan(u16(*get_key)(void));  //硬件按键扫描
    }
}
int main(int argc,char* args[])
{

    if (!init_SDL2()) return -1;
    SDL_CreateThread(tick_thread, "tick", NULL);    //定时线程

    SC_GUI_Init(SDL_DrawPoint,C_BLACK,C_RED,C_WHEAT,C_GREEN,(lv_font_t*)&myFont_12_4bpp);

    sc_create_task(TASK_ID_MAIN,task_main_cb,50);    //主线程，处理菜单
    sc_send_sig(TASK_ID_MAIN, SIG_INIT);

    while (1)
    {
        SDL_Delay(1);   /*Sleep for 5 millisecond*/
        g_tick++;
        sc_task_loop();
        if(g_tick%30==0)
        {
            up_lcd();
        }
        if(get_sdl2_key()==-1)
        {
            return -1;
        }
    }
    close_SDL2();
    return 0;
}

//中断函数模拟5ms
int tick_thread(void * data)
{
    (void)data;
    while(1)
    {
        SDL_Delay(5);        /*Sleep for 5 millisecond*/
    }
    return 0;
}

