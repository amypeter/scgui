# SCGUI

#### 介绍
/*
*                        SC_GUI for V1.1
* 一个小内存(1-2k)的开源GUI,支持LVGL抗锯齿字体,控件透明度，动态文字，实用波形图,简易图层叠加，支持双DMA,移植简单直接绘制所见即所得
* 感谢网友开源工具Lvgl Font Tool V0.4 生成 阿里(qq:617622104)
* 作者:黎R(WX:13018616872)
* 社区:(QQ群:799501887)
*/

#### 软件架构
软件架构说明


#### 安装教程

```
//sc_lcd.c
//底层接口，与lvgl接口一样。
void LCD_DMA_color(int xs, int ys, int xe, int ye, u16 *color)
{
#if 0
    SPI_DMA_color( xs,  ys,  xe,  ye,color);
#else
    //-----------------无DMA------------------------
    // LCD_SetWindows(xs, ys,xe, ye);
    for(int x = xs; x <=xe; x++)
    {
        for(int y = ys; y <=ye; y++)
        {
            gui->bsp_pset (x,y,*color);
            // LCD_Write_Dat16(*color);	//写入数据
            color++;
        }
    }
#endif
}
int main(int argc,char* args[])
{

    extern void Lcd_DrawPoint(int x,int y,u16 c);
    SC_GUI_Init(Lcd_DrawPoint,C_BLACK,C_RED,C_WHEAT,C_GREEN,(void*)&myFont_12);
    
    while (1)
    {

    }

}

```



#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
