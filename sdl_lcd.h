

#ifndef SDL_LCD__H
#define SDL_LCD__H

#include  <SDL2/SDL.h>
#include  <SDL2/SDL_keyboard.h>
#include  <SDL2/SDL_keycode.h>
#include  <stdio.h>

int init_SDL2(void);
void SDL_DrawPoint ( int16_t x, int16_t y, uint16_t c );
int get_SDL_Event(void);
void up_lcd(void);

#endif // SDL_LCD__H
